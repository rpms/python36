# ==================
#     IMPORTANT
# ==================

#      This package is intended only for the RHEL8 buildroot, and therefore
#      it's blocked from going into the rhel-8.0-candidate tag.
#      See: https://projects.engineering.redhat.com/browse/RCM-37705
#
#      To build it, use the `-skip-tag` option and then tag the build into
#      the `rhel-8.0-override` tag.






# ==================
# Top-level metadata
# ==================

%global pybasever 3.6

Name:       python36
Version:    %{pybasever}.6
Release:    13.buildroot%{?dist}
Summary:    Interpreter of the Python programming language

License:    Python
URL:        https://www.python.org/
Source:     https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
%global debug_package %{nil}

# ==================================
# Conditionals controlling the build
# ==================================

# Extra build for debugging the interpreter or C-API extensions
# (the -debug subpackages)
%bcond_without debug_build


# ==============================================
# Metadata and macros from the python3 component
# ==============================================

# ABIFLAGS, LDVERSION and SOABI are in the upstream configure.ac
# See PEP 3149 for some background: http://www.python.org/dev/peps/pep-3149/
%global ABIFLAGS_optimized m
%global ABIFLAGS_debug     dm

%global LDVERSION_optimized %{pybasever}%{ABIFLAGS_optimized}
%global LDVERSION_debug     %{pybasever}%{ABIFLAGS_debug}


# =======================
# Package and subpackages
# =======================

# python36 package itself
Provides: python3 = %{version}-%{release}
Provides: python3%{?_isa} = %{version}-%{release}
Obsoletes: python3 < 3.6.6-13

Requires:   %{_libexecdir}/platform-python

%global _description \
Python is an accessible, high-level, dynamically typed, interpreted programming\
language, designed with an emphasis on code readibility.\
It includes an extensive standard library, and has a vast ecosystem of\
third-party libraries.\
\
The python36 package provides the "python3.6" executable: the reference\
interpreter for the Python language, version 3.\
\
The python36-devel package contains files for dovelopment of Python application\
and the python36-debug is helpful for debugging.\
\
Packages containing additional libraries for Python 3.6 are generally named\
with the "python3-" prefix. Documentation for Python is provided in the\
python3-docs package.

%description %_description


%package devel
Summary:    Libraries and header files needed for Python development
Requires:   python36
Requires:   platform-python-devel

%description devel
This package contains the header files and configuration needed to compile
Python extension modules (typically written in C or C++), to embed Python
into other programs, and to make binary distributions for Python libraries.

It also contains the necessary macros to build RPM packages with Python modules.


%if %{with debug_build}
%package debug
Summary:    Debug version of the Python runtime
Provides:   python3-debug = %{version}-%{release}
Provides:   python3-debug%{?_isa} = %{version}-%{release}
Obsoletes:  python3-debug < 3.6.6-13
Requires:   python36
Requires:   python36-devel
Requires:   platform-python-debug

%description debug
python3-debug provides a version of the Python runtime with numerous debugging
features enabled, aimed at advanced Python users such as developers of Python
extension modules.

This version uses more memory and will be slower than the regular Python build,
but is useful for tracking down reference-counting issues and other bugs.

The bytecode format is unchanged, so that .pyc files are compatible between
this and the standard version of Python, but the debugging features mean that
C/C++ extension modules are ABI-incompatible and must be built for each version
separately.

The debug build shares installation directories with the standard Python
runtime, so that .py and .pyc files can be shared.
Compiled extension modules use a special ABI flag ("d") in the filename,
so extensions for both verisons can co-exist in the same directory.
%endif # with debug_build


%prep
%autosetup -n Python-3.6.3


%build


%install
mkdir -p %{buildroot}%{_bindir}

# Symlink the executable to libexec
ln -s %{_libexecdir}/platform-python%{pybasever} %{buildroot}%{_bindir}/python%{pybasever}
# Additional symlink for buildroot
ln -s %{_bindir}/python%{pybasever} %{buildroot}%{_bindir}/python3

# Symlink the config executables
ln -s python%{LDVERSION_optimized}-config %{buildroot}%{_bindir}/python%{pybasever}-config
# Additional symlink for buildroot
ln -s %{_bindir}/python%{pybasever}-config %{buildroot}%{_bindir}/python3-config

# LDVERSION specific section
InstallPython() {
  LDVersion=$1

  # Symlink python*-config to libexec
  ln -s %{_libexecdir}/platform-python${LDVersion}-config %{buildroot}%{_bindir}/python${LDVersion}-config
  ln -s %{_libexecdir}/platform-python${LDVersion}-`uname -m`-config %{buildroot}%{_bindir}/python${LDVersion}-`uname -m`-config

  # Symlink optimized and debug executables
  ln -s %{_libexecdir}/platform-python${LDVersion} %{buildroot}%{_bindir}/python${LDVersion}
}

%if %{with debug_build}
InstallPython %{LDVERSION_debug}
# General debug symlinks
ln -s python%{LDVERSION_debug}-config %{buildroot}%{_bindir}/python%{pybasever}-debug-config
ln -s python%{LDVERSION_debug} %{buildroot}%{_bindir}/python%{pybasever}-debug
# Additional symlinks for buildroot
ln -s %{_bindir}/python%{pybasever}-debug-config %{buildroot}%{_bindir}/python3-debug-config
ln -s %{_bindir}/python%{pybasever}-debug %{buildroot}%{_bindir}/python3-debug
%endif # with debug_build

InstallPython %{LDVERSION_optimized}


%files
%license LICENSE
%doc README.rst
%{_bindir}/python%{pybasever}
%{_bindir}/python%{LDVERSION_optimized}
%{_bindir}/python3

%files devel
%license LICENSE
%doc README.rst
%{_bindir}/python%{pybasever}-config
%{_bindir}/python%{LDVERSION_optimized}-config
%{_bindir}/python%{LDVERSION_optimized}-*-config
%{_bindir}/python3-config

%files debug
%license LICENSE
%doc README.rst
%{_bindir}/python%{LDVERSION_debug}
%{_bindir}/python%{LDVERSION_debug}-config
%{_bindir}/python%{LDVERSION_debug}-*-config
%{_bindir}/python%{pybasever}-debug-config
%{_bindir}/python%{pybasever}-debug
%{_bindir}/python3-debug
%{_bindir}/python3-debug-config


#               IMPORTANT              #
# See note at the top of the spec file #
%changelog
* Sat Sep 29 2018 Tomas Orsava <torsava@redhat.com> - 3.6.6-13.buildroot
- Merged `stream-3.6` branch into the `rhel-8.0` branch for the buildroot
- Differences:
  - Don't provide `python3-devel` from the `python36-devel` package because
    `python3-devel` itself is actually available in the buildroot
  - Instead of using the `alternatives`, simply simplink the main executables
    (python3, python3-config,...)
- Resolves: rhbz#1619153

* Tue Aug 07 2018 Tomas Orsava <torsava@redhat.com> - 3.6-5
- Bump

* Tue Jul 31 2018 Tomas Orsava <torsava@redhat.com> - 3.6-4
- Add python3{,-config,-debug} symlinks specially for the buildroot

* Tue Jul 31 2018 Tomas Orsava <torsava@redhat.com> - 3.6-3
- Bump release after revert of commits

* Wed Apr 04 2018 Tomas Orsava <torsava@redhat.com> - 3.6-1
- This new package python36 will belong to the python36 module that will house
  symlinks from /usr/bin/python* to /usr/libexec/platform-python* et al.

